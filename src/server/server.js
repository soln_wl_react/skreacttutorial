var express = require('express');
var graphqlHTTP = require('express-graphql');
var fetch = require('node-fetch');
var { buildSchema } = require('graphql');
var cors = require('cors');

var schema = buildSchema(`
  type Query {
    hello: String,
    test: testdata
  },
  type testdata{
    status : String
    message : String
  }
`);

var root = { 
  hello: () => 'Hello world!',
  test: async () => {
    var data = await fetch('https://dog.ceo/api/breeds/image/random');
    return data.json();
  }  

};
var app = express();
app.use('/graphql', cors(), graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: false,
}));

app.use('/graphiql',cors(), graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(5000, () => console.log('Now browse to localhost:5000/graphiql'));
// node index.js => http://localhost:5000/graphql?query={hello}