import React from 'react';
import styled from 'styled-components'
import Reacticon from './atoms/icons/icons'
import Labeltext from './atoms/text/text'

const Logowrapper = styled.div `
width: 30px;
margin: 16px 0px 0px 10px;
` 
class Header extends React.Component
{
    render() {
        return (
            <div className="headerCont">
                <Logowrapper><Reacticon/></Logowrapper>                
                <Labeltext className="labelstr" text="MERN TUTORIAL" />                
            </div>
        )
    }    
}

class Footer extends React.Component
{
    render() {
        return (
            <div className="footerCont">                
                <Logowrapper><Reacticon/></Logowrapper>
                <Logowrapper><Reacticon/></Logowrapper>
                <Logowrapper><Reacticon/></Logowrapper>               
            </div>
        )
    }    
}


export default Header
export { Footer }
