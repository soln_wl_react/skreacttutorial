const mongoDB =[
    {
        title : "home",
        content : [
            { title : "MongoDB Tutorial", desc : "MongoDB is an open-source document database and leading NoSQL database. MongoDB is written in C++. This tutorial will give you great understanding on MongoDB concepts needed to create and deploy a highly scalable and performance-oriented database."},
            { title : "Audience", desc : "This tutorial is designed for Software Professionals who are willing to learn MongoDB Database in simple and easy steps. It will throw light on MongoDB concepts and after completing this tutorial you will be at an intermediate level of expertise, from where you can take yourself at higher level of expertise."},
            { title : "Prerequisites", desc : "Before proceeding with this tutorial, you should have a basic understanding of database, text editor and execution of programs, etc. Because we are going to develop high performance database, so it will be good if you have an understanding on the basic concepts of Database (RDBMS)."}
        ]
    },
    {
        title : "overview",
        content : [
            { title : "", desc : "MongoDB is a cross-platform, document oriented database that provides, high performance, high availability, and easy scalability. MongoDB works on concept of collection and document."},
            { title : "Database", desc : "Database is a physical container for collections. Each database gets its own set of files on the file system. A single MongoDB server typically has multiple databases." },
            { title : "Collection", desc : "Collection is a group of MongoDB documents. It is the equivalent of an RDBMS table. A collection exists within a single database. Collections do not enforce a schema. Documents within a collection can have different fields. Typically, all documents in a collection are of similar or related purpose." },
            { title : "Document", desc : "A document is a set of key-value pairs. Documents have dynamic schema. Dynamic schema means that documents in the same collection do not need to have the same set of fields or structure, and common fields in a collection's documents may hold different types of data." },
            { 
                title : "Sample Document", 
                desc : "_id is a 12 bytes hexadecimal number which assures the uniqueness of every document. You can provide _id while inserting the document. If you don’t provide then MongoDB provides a unique id for every document. These 12 bytes first 4 bytes for the current timestamp, next 3 bytes for machine id, next 2 bytes for process id of MongoDB server and remaining 3 bytes are simple incremental VALUE.", 
                program : `<pre class="prettyprint notranslate prettyprinted" style="">
                <span class="pun">{</span>
                        <span class="pln">
                    _id</span><span class="pun">:</span><span class="pln"> </span><span class="typ">ObjectId</span><span class="pun">(</span><span class="lit">7df78ad8902c</span><span class="pun">)</span><span class="pln">
                    title</span><span class="pun">:</span><span class="pln"> </span><span class="str">'MongoDB Overview'</span><span class="pun">,</span><span class="pln"> 
                    description</span><span class="pun">:</span><span class="pln"> </span><span class="str">'MongoDB is no sql database'</span><span class="pun">,</span><span class="pln">
                    </span><span class="kwd">by</span><span class="pun">:</span><span class="pln"> </span><span class="str">'tutorials point'</span><span class="pun">,</span><span class="pln">
                    url</span><span class="pun">:</span><span class="pln"> </span><span class="str">'http://www.tutorialspoint.com'</span><span class="pun">,</span><span class="pln">
                    tags</span><span class="pun">:</span><span class="pln"> </span><span class="pun">[</span><span class="str">'mongodb'</span><span class="pun">,</span><span class="pln"> </span><span class="str">'database'</span><span class="pun">,</span><span class="pln"> </span><span class="str">'NoSQL'</span><span class="pun">],</span><span class="pln">
                    likes</span><span class="pun">:</span><span class="pln"> </span><span class="lit">100</span><span class="pun">,</span><span class="pln"> 
                    comments</span><span class="pun">:</span><span class="pln"> </span><span class="pun">[</span><span class="pln">	
                    </span><span class="pun">{</span><span class="pln">
                        user</span><span class="pun">:</span><span class="str">'user1'</span><span class="pun">,</span><span class="pln">
                        message</span><span class="pun">:</span><span class="pln"> </span><span class="str">'My first comment'</span><span class="pun">,</span><span class="pln">
                        dateCreated</span><span class="pun">:</span><span class="pln"> </span><span class="kwd">new</span><span class="pln"> </span><span class="typ">Date</span><span class="pun">(</span><span class="lit">2011</span><span class="pun">,</span><span class="lit">1</span><span class="pun">,</span><span class="lit">20</span><span class="pun">,</span><span class="lit">2</span><span class="pun">,</span><span class="lit">15</span><span class="pun">),</span><span class="pln">
                        like</span><span class="pun">:</span><span class="pln"> </span><span class="lit">0</span><span class="pln"> 
                    </span><span class="pun">},</span><span class="pln">
                    </span><span class="pun">{</span><span class="pln">
                        user</span><span class="pun">:</span><span class="str">'user2'</span><span class="pun">,</span><span class="pln">
                        message</span><span class="pun">:</span><span class="pln"> </span><span class="str">'My second comments'</span><span class="pun">,</span><span class="pln">
                        dateCreated</span><span class="pun">:</span><span class="pln"> </span><span class="kwd">new</span><span class="pln"> </span><span class="typ">Date</span><span class="pun">(</span><span class="lit">2011</span><span class="pun">,</span><span class="lit">1</span><span class="pun">,</span><span class="lit">25</span><span class="pun">,</span><span class="lit">7</span><span class="pun">,</span><span class="lit">45</span><span class="pun">),</span><span class="pln">
                        like</span><span class="pun">:</span><span class="pln"> </span><span class="lit">5</span><span class="pln">
                    </span><span class="pun">}</span><span class="pln">
                    </span><span class="pun">]</span><span class="pln">
                </span><span class="pun">}</span></pre>`
            }
        ]
    },
    {
        title: "MongoDB - Advantages",
        content : [
            {title: "", desc:"Any relational database has a typical schema design that shows number of tables and the relationship between these tables. While in MongoDB, there is no concept of relationship."},
            {title: "Advantages of MongoDB over RDBMS", 
                content: [
                    
                ]
            },
            {title: "", desc: ""},
            {title: "", desc: ""}
        ]
    }
]

export default mongoDB;