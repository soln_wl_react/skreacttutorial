import React from 'react'
import styled from 'styled-components'

const ProgramCont = styled.code`
    border: 1px solid #d6d6d6;
    background-color: #eee;
    float: left;
    width: calc(100% - 7rem);
    border-radius: 0;
    padding: 5px;
    margin: 0;
    margin-bottom: 10px;
    margin-top: 15px;
    margin-left: 50px;
    font-size: 13px;
    overflow: auto;
`;

class Code extends React.Component{
    render(){
        return(
            <ProgramCont dangerouslySetInnerHTML={{__html: this.props.code}}></ProgramCont>
        );
    }
}

export default Code;