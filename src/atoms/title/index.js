import React from 'react'
import styled from 'styled-components'

const T1 = styled.div`
    font-size: 30px;
    color: #15b23b;
    text-align: center;
    width: 100%;
    padding: 10px 0px;
    font-family: arial;
`;

const T2 = styled.div`
    font-size: 20px;
    color: #125391;
    text-align: left;
    padding: 8px 10px;
    font-family: arial;
`;

class Title extends React.Component{
    render(){
        return(
            <React.Fragment>
                <T1>{this.props.title}</T1>
                {this.props.children}
            </React.Fragment>
        );
    }
}

class SubTitle extends React.Component{
    render(){
        return(
            <React.Fragment>
                <T2>{this.props.title}</T2>
                {this.props.children}
            </React.Fragment>
        );
    }
}

export {Title, SubTitle}