import React from 'react'
import '../../App.css'

export default class Labeltext extends React.Component
{
    render()
    {
      return(
        <span className={this.props.className}>{this.props.text}</span>
      )
    }
}