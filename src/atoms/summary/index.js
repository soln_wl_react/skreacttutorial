import React, {Fragment} from 'react'
import styled from 'styled-components'

const Paragraph = styled.div`
    font-size: 16px;
    font-family: arial;
    padding: 10px 10px 10px 50px;
    color: #070000;
`;

const UL = styled.ul`
    list-style-type: none;
    font-size: 18px;
    font-family: arial;
    padding: 10px 10px 10px 50px;
    color: #070000;
    margin-top: 0px;
`;

const LI = styled.li`
    padding: 10px 0px;
`;

class Summary extends React.Component{
    render(){
        return(
            <Paragraph>{this.props.description}</Paragraph>
        );
    }
}

class ListedSummary extends React.Component{

    render(){
        const list = (content) =>{
            if(content.title && content.desc)
            {
                return <LI ><b>{content.title}</b> - {content.desc}</LI>
            }
            return <LI >{content.desc}</LI>
        }

        return(
            <Fragment>
                <UL>{this.props.content.map(list)}</UL>
            </Fragment>
        );
    }
}

export { Summary, ListedSummary };