import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Template from './views/layouts/layout';

ReactDOM.render(<Template />, document.getElementById('root'));

