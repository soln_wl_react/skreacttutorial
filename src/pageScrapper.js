// import rp from "request-promise"
// import cheerio from "cheerio"
// import React, {Component} from "react"

const react = require('react')
const rp = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
process.env.UV_THREADPOOL_SIZE = 256;
const scrapeURIConfig = {
    defaultDomain : "https://www.tutorialspoint.com",
    nodejs : "/nodejs/index.htm"
}

const fileWriter = function(data){
    let cbk = function(){console.log("File written")}
    fs.writeFile('scrapeData.json', JSON.stringify(data, null, 4), 'utf8', cbk);
}


class PageScrapper extends react.Component
{
    constructor(props) {
        super(props);
        this.parsedContent = []
        this.getOptions = this.getOptions.bind(this)
        this.parseTitlesDescriptions = this.parseTitlesDescriptions.bind(this)
        this.parseMenu = this.parseMenu.bind(this)
        this.contentScraper = this.contentScraper.bind(this)
        this.combineTitleDescription = this.combineTitleDescription.bind(this)
        this.scrapeContent = this.scrapeContent.bind(this);
    }

    getOptions (pageName,pageLink) {
        let scrapeURI = (pageName && typeof scrapeURIConfig[pageName] != "undefined")? scrapeURIConfig[pageName]: ((pageLink)? pageLink: null)
        if(scrapeURI)
        {
            scrapeURI = scrapeURIConfig.defaultDomain + scrapeURI
            console.log(scrapeURI)
            let options = {
                uri:scrapeURI,
                transform: function (body) {
                    return cheerio.load(body)
                },
                resolveWithFullResponse: true,
                transform2xxOnly: true,
                simple: false,
                timeout: 600000
            }
            return options;
        }
        else
        {
            console.log("ScrapeURI is missing")
            return
        }
    }

    contentScraper (options, cbk, dataCbk) {
        rp(options)
            .then(($) => {
                cbk($, dataCbk)
            })
            .catch((err) => {
                console.log(err);
            });
    }

    parseTitlesDescriptions ($, cbk) {
        //Parsing sub links of side menu
        let parsedTitles = [];
        let parsedDescriptions = [];

        let H1Elements = $(".content .middle-col h1");
        let H2Elements = $(".content .middle-col h2");

        if($(H1Elements).length || $(H2Elements).length)
        {
            //Parsing titles
            let titleElement = ($(H1Elements).length > $(H2Elements).length)? $(H1Elements): $(H2Elements)
           // $(titleElement).each(function(i,el){console.log($(el).text())})
            titleElement.each(function(i,el){parsedTitles.push($(el).text())})
            
            //Parsing descriptions
            $(".content .middle-col p").each(function(i,el){parsedDescriptions.push($(el).text())})

            cbk(this.combineTitleDescription(parsedTitles, parsedDescriptions))
        }
        else
            console.log("Unable to parse titles and descriptions");
    }

    combineTitleDescription(titles, descriptions) {
        let combinedTitleDesc = [];
        let n = (titles.length < descriptions)? titles.length: descriptions.length
        for(let i=0 ; i<n ; i++)
        {
            let combinedMenuObj = {}
            combinedMenuObj.title = titles[i]
            combinedMenuObj.description = descriptions[i]
            combinedTitleDesc.push(combinedMenuObj);
        }
        //console.log(JSON.stringify(combinedTitleDesc));
        return combinedTitleDesc;
    }

    parseMenu($) {
        let self = this;
        let parsedContent = [];
        let sidemenuContainers = $(".sidebar ul.left-menu");
        let mainSideMenuContainer = $($(sidemenuContainers)[0]).find("a");
        $(sidemenuContainers).each(function(i, el){
            if(i != 0 && ($(el).find("a").length > $(mainSideMenuContainer).length))
            {
                mainSideMenuContainer = $(el).find("a");
            }
        });

        //Parsing side menu
        if(mainSideMenuContainer.length)
        {
           $(mainSideMenuContainer).each(async function(i,el){
                let menuObj = {}
                menuObj.menuText = $(this).text()
                menuObj.menuLink = $(this).attr("href")
                self.contentScraper(self.getOptions(null, menuObj.menuLink),self.parseTitlesDescriptions,function(data){ 
                    menuObj.menuContent = data;
                    if(menuObj.menuContent)
                    {
                        self.parsedContent.push(menuObj)
                        //console.log(self.parsedContent);
                        fileWriter(self.parsedContent);
                    }
                    else
                    {
                        console.log("No Content")
                    }
                })
            })
        }
        //console.log(parsedContent);
    }

    scrapeContent () {
        //Initital page scraping with the link in scrapeURIConfig obj
        this.contentScraper(this.getOptions(this.props.pageName),this.parseMenu)
    }

    render()
    {  
        this.scrapeContent();
    }
}

var obj = new PageScrapper({"pageName":"nodejs"});
obj.render();

/*ReadME
RUN yarn to install "fs" package
Navigate to /src and run "node pageScrapper.js" to run this file individually
Execution of this code started manually for debuggin purposes (ie) from `obj.render()`
    * PageName has to be passed as object as above `obj = new PageScrapper({"pageName":"nodejs"});`
    * Based on the value passed through pageName parameter, corresponding initial page URL will be identified for scraping from `scrapeURIConfig` object declared at the top
Current Status: Data Scraped and data object written to scrapeData.json
Note: To avoid frequent scraping, Included a file named scrapeData.json in /src and wrote a function fileWriter() here to write the finally scraped json data to the file and so we can fetch the data from that file to render MERN page components
*/