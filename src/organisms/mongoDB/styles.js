import styled from 'styled-components'

const headerHeight = 68, footerHeight = 68;
const MainContHeight = (window.innerHeight - (headerHeight + footerHeight));

const MainCont = styled.div `
  width: calc(100% - 150px);
  display: flex;
  flex-direction: column;
  height: ${MainContHeight}px;
  background: #fff;
  flex-direction: column;
  float: left;
  overflow-y: scroll;
`

export {MainCont}