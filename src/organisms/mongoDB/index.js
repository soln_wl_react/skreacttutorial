import React from 'react'

import TitleAndDesc from '../../molecules/title_desc'
import { Title } from '../../atoms/title'
import { MainCont } from './styles'

class Mongo extends React.Component{
    render(){
        let renderDesc = (content) => {
            return <TitleAndDesc title={content.title} description={content.desc} code={content.program}/>
        }
        let renderPageContents = (data) => {
            return <Title title={data.title} children={data.content.map(renderDesc)}/>
        }
        return(
            <MainCont>
                {this.props.data.map(renderPageContents)}
            </MainCont>
        )
    }
}

export default Mongo