import React from 'react'

import { MainCont } from './style'
import { Title, SubTitle } from '../../atoms/title'
import { Summary, ListedSummary } from '../../atoms/summary'
import { home } from '../../state/home'


class HomeContainer extends React.Component{
    render(){
        return(
            <MainCont>
                <Title title={home.title} />
                <SubTitle title={home.introduction} >
                    <Summary description={home.description}/>
                </SubTitle>
                <SubTitle title={home.prerequisite.title} >
                    <ListedSummary content={home.prerequisite.content}/>
                </SubTitle>
            </MainCont>
        );
    }
}

export default HomeContainer