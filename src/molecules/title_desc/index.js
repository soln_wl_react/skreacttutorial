import React, {Component} from 'react'

import {SubTitle} from '../../atoms/title'
import {Summary} from '../../atoms/summary'
import Code from '../../atoms/code'

class TitleAndDesc extends Component{
    render(){
        return(
            <div>
                <SubTitle title = { this.props.title }/>
                <Summary description = { this.props.description }/>
                {this.props.code ? <Code code = {this.props.code} /> : ''}
                
            </div>
        );
    }
}

export default TitleAndDesc;