import React from 'react'
import '../App.css'
import styled from 'styled-components'
import { Link } from "react-router-dom";
import {Homeicon, Nodejsicon, Expressicon, ES6icon, MongoDBicon, Packageicon, Reacticon} from '../atoms/icons/icons'

const headerHeight = 68, footerHeight = 68;
const sideBarHeight = (window.innerHeight - (headerHeight + footerHeight));
const sideBarListHeight = sideBarHeight/6;
const SideBarCont = styled.div `
  width: 150px;
  display: flex;
  flex-direction: column;
  height: ${sideBarHeight}px;
  background: #fdce89;
  flex-direction: column;
    @media (max-width: 372px)
    {
      display:none;
    }
  float: left;
`

const Menuwrapper = styled.div `
  width: 150px;
  border-bottom: 2px solid #fff;
  height:${sideBarListHeight}px;
  align-items: center;
  display: flex;
  justify-content: center;
  svg{
    width: 125px;
    height: 70px;
  }
  &:hover {
    background: #ff9800;
  }
` 


export default class Sidebar extends React.PureComponent {
  currentPage = window.location.href.split("/")[0].trim();
  render() {
    return (
        <SideBarCont>
            <Link to="/home"><Menuwrapper isActive={this.currentPage == "home" && true}><Homeicon /></Menuwrapper></Link>
            <Link to="/nodejs"><Menuwrapper isActive={this.currentPage == "nodejs" && true}><Nodejsicon/></Menuwrapper></Link>
            <Link to="/react"><Menuwrapper isActive={this.currentPage == "react" && true}><Reacticon/></Menuwrapper></Link>
            <Link to="/ecma"><Menuwrapper isActive={this.currentPage == "ecma" && true}><ES6icon/></Menuwrapper></Link>
            <Link to="/mongodb"><Menuwrapper isActive={this.currentPage == "mongodb" && true}><MongoDBicon/></Menuwrapper></Link>
            <Link to="/packages"><Menuwrapper isActive={this.currentPage == "packages" && true}><Packageicon/></Menuwrapper></Link>
        </SideBarCont>
    )
  }
}

