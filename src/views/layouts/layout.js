import React from 'react'
import Header, {Footer} from '../.././headerFooter'
import Sidebar from '../../molecules/sidebar'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import styled from 'styled-components'
import * as Pages from "../pages/index"
import ApolloClient, { gql } from "apollo-boost";
import { ApolloProvider, Query, graphql } from "react-apollo";

const Wrapper = styled.div `
  display: flex;
  flex-flow: row wrap;  
`
const client = new ApolloClient({
    uri: `//localhost:5000/graphql` //use localhost:4000 for local graphql server
});

class Template extends React.Component
{
    render()
    {
        return (
            <ApolloProvider client={client}>
            <Router>
                <Wrapper>
                    <Header />
                    <Sidebar />
                    <Switch>
                        <Route exact path='/home' component={Pages.Home} />
                        <Route exact path='/nodejs' component={Pages.Nodejstutorial} />
                        <Route exact path='/ecma' component={Pages.Ecmatutorial} />
                        <Route exact path='/react' component={Pages.Reacttutorial} />
                        <Route exact path='/mongodb' component={Pages.Mongodbtutorial} />
                        <Route exact path='/packages' component={Pages.Packagestutorial} />
                    </Switch>
                    <Footer />
                </Wrapper>
            </Router>
            </ApolloProvider>
        );
    }
}

export default Template