import Home from "./home"
import Ecmatutorial from "./ecma"
import Mongodbtutorial from "./mongodb"
import Nodejstutorial from "./nodejs"
import Reacttutorial from "./reactapp"
import Packagestutorial from "./packages"

export {Home, Ecmatutorial, Mongodbtutorial, Nodejstutorial, Reacttutorial, Packagestutorial}