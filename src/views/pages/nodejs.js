import React from 'react'
import gql from "graphql-tag";
import { Query } from "react-apollo";

const GET_NODEQUERY = gql`
  {
    test {
      status
      message
    }
  }
`;

class Nodejstutorial extends React.Component
{
    render() {
        return (
            <Query query={GET_NODEQUERY}>
                {({ loading, error, data }) => {
                if (loading) return "Loading...";
                if (error) return `Error! ${error.message}`;
                return (
                    <div>
                        <div>{data.test.status}</div>
                        <img src={data.test.message} />  
                    </div>
                    );
                }}
            </Query>
        )
    }    
}


export default Nodejstutorial