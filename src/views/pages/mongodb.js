import React from 'react'

import Mongo from '../../organisms/mongoDB'
import mongoDB from '../../state/mongoDB'

class Mongodbtutorial extends React.Component
{
    render() {
        return (
            <Mongo data={mongoDB}/>
        )
    }    
}


export default Mongodbtutorial