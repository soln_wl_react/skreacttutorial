var express = require('express');
var fs = require('fs');
var rp = require('request-promise');
var cheerio = require('cheerio');
var app = express();
var ip = require('ip');
var url = require('url');

var router = express.Router();

//Mongo DB Connectivity
var mongoDBname = "scrape";
mongoDBname = "mern-scrape";
var MongoClient = require('mongodb').MongoClient;
var mongoDBurl = 'mongodb://localhost:27017/'+mongoDBname;
mongoDBurl = 'mongodb://sample:sample@ds121960.mlab.com:21960/mern-scrape';
MongoClient.connect(mongoDBurl, function(err, db) {
  if (err) console.log(err);
  console.log("Database created!");
  db.close();
});  
// MongoClient.connect(mongoDBurl, function(err, db) {
//   if (err) throw err;
//   console.log("Database created!");
//   db.close();
// });
var DROPCOLLECTION = false, SCRAPEPAGE = false;
if(DROPCOLLECTION){	
	MongoClient.connect(mongoDBurl, function(err, db) {
		if (err) throw err;
		var dbo = db.db(mongoDBname);
		dbo.listCollections().toArray(function(err, collections){
			console.log("collections");
			console.log(collections)
			if(collections && collections.length){
				var ci = -1, collectionLen = collections.length;
				dropCollectionLoop();
				function dropCollectionLoop(){
					ci++;
					if(ci == collectionLen){
						console.log("All collectoion removed");
					}else{
						dropCollection()
					}
				}
				
				function dropCollection(){
					var cName = collections[ci].name;
					if(languageNames.indexOf(cName) > -1){
						// console.log("cName")
						// console.log(cName)
						dbo.collection(cName).drop(function(err, delOK) {
							if (err) throw err;
							if (delOK) console.log(cName + " Collection deleted");
							dropCollectionLoop();
						});
					}
					else{
						dropCollectionLoop
					}
				}
			}
		});
	});
	console.log("test colloection drop")
}

//Scraping Config
var dir = "", languageName;
const DOMAIN = 'https://www.tutorialspoint.com'
const options = {
  uri: ``,
  transform2xxOnly: true,
  simple: false,
  transform: function (body) {
    return cheerio.load(body);
  }
};

var listLen, ii=-1, list, pageTitle;
var languageNames =[/*"es6",*/ "expressjs", "reactjs", "nodejs", "mongodb"] 
var jj = -1;

router.get('/getsidebar', function (req, res) {
	//console.log(req);
	var q = url.parse(req.url, true);
	var languageName = q.query.lang;
	console.log(q.query.lang);
	
	MongoClient.connect(mongoDBurl, function(err, db) {
		if (err) throw err;
		var dbo = db.db(mongoDBname);
		var query = { "type": "sidebar" }
		dbo.collection(languageName).find(query).toArray(function(err, result) {
		  if (err) throw err;
		  console.log(result);
		  res.send(result)
		  db.close();
		});
	});
});

router.get('/getpagedata', function (req, res) {
	//console.log(req);
	var q = url.parse(req.url, true);
	var languageName = q.query.lang;
	var topic = q.query.topic;
	console.log(q.query.lang);
	
	MongoClient.connect(mongoDBurl, function(err, db) {
		if (err) throw err;
		var dbo = db.db(mongoDBname);
		var query = { "title": topic };
		dbo.collection(languageName).find(query).toArray(function(err, result) {
		  if (err) throw err;
		  console.log(result);
		  res.send(result)
		  db.close();
		});
	});
});

var defaultPath = __dirname;
app.use(express.static(defaultPath));

app.use('/api', router);

app.get('/scrape', function(req, res){
	if(SCRAPEPAGE){
		languageLen = languageNames.length;
		function languageListLoop(fromPar){
			if(fromPar && (jj > -1)){
	
			}else{
				jj++;
				ii=-1;
				console.log("languageListLoop")
				console.log("ii   :    "+ii+"    jj   :   "+jj)
				if(jj == languageLen){
					console.log("============== Finiched ==============")
					res.send('Check your console!')
				}else{
					languageLists();
				}
			}
		}
		function languageLists(){
			console.log("languageLists")
			languageName = languageNames[jj];
			console.log("languageName   : "+ languageName)
			options.uri = "https://www.tutorialspoint.com/" + languageName + "/index.htm";
			dir = "./output/"+languageName+"/";
			// console.log(i+"    :   "+ options.uri);
			MongoClient.connect(mongoDBurl, function(err, db) {
				if (err) throw err;
				var dbo = db.db(mongoDBname);
				console.log("languageName   :   "+ languageName)
				dbo.createCollection(languageName, function(err, res) {
				  if (err) throw err;
				  console.log(languageName + " Collection created!");
				  db.close();
				});
			});
			rp(options).then(function (data) {
				var langCallBack = function(){
					console.log("langCallBack end")
					languageListLoop();
				}
				console.log("Language Page scraper")
				getSideBarUrl(data, res, langCallBack);	
			}).catch(function (err) {
				console.log(err);      
			});
		}
		console.log("call languageListLoop")
		languageListLoop(true);
	}else{
		// res.send("Check console");
		res.sendFile(__dirname+"/resp.html");
	}
})


function getSideBarUrl($, res, langCallBack){
	console.log("getSideBarUrl")
	var json = { };
    var data =$(".sidebar .nav-list.primary").eq(0);
	json.list = [];
	if(data.children().length < 3){
		data =$(".sidebar .nav-list.primary").eq(1);
	}
    data.children().each(function(i, elem) {
      if(i==0){
        // console.log(i+ "  true")
        json.title = $(elem).text()
      }else{
        var text = $(elem).text()
        var aHref = $(elem).find("a").attr("href");
        var obj = {}
        obj.title = text;
		obj.url = aHref;
		if(!aHref){
			return false;
		}else{
			json.list.push(obj);
		}
      }
	});
	if (!fs.existsSync(dir)){
		fs.mkdirSync(dir);
	}
	//Generate Side Bar JSON

	fs.writeFile(dir+languageName+'.json', /*json*/JSON.stringify(json, null, 4), function(err){
		console.log('File successfully written! - Check your project directory for the output.json file');      
	});
	list = [], listLen = 0;
	MongoClient.connect(mongoDBurl, function(err, db) {
		if (err) throw err;
		var dbo = db.db(mongoDBname);
		var myobj = { type : "sidebar", value: json};
		dbo.collection(languageName).insertOne(myobj, function(err, res) {
			if (err) throw err;
			console.log(languageName + " sidebar document inserted");
			db.close();
			console.log()
			console.log()
			getEveryPageData(json.list, res, langCallBack);
		});
	});
	// res.send("success")
	
}

function getEveryPageData(lst, res, langCallBack){
	listLen = lst.length;
	console.log("listLen")
	console.log(listLen)
	console.log("getEveryPageData")
	list = lst;
	var callBack = function(){
		langCallBack();
	};
	getAllLinkDataFromLangLoop(callBack)
}
	
function getAllLinkDataFromLangLoop(callBack){
	console.log("getAllLinkDataFromLangLoop")
	ii++;
	console.log("ii   "+ ii)
	if(ii == listLen){
		callBack()
	}else{
		getAllLinkDataFromLang(ii, callBack)
	}
}

function getAllLinkDataFromLang(i, callBack){
	console.log("getAllLinkDataFromLang")
	obj = {};
	obj = list[i];
	pageTitle = obj.title;
	options.uri = DOMAIN + obj.url;
	fName = obj.title;
	// console.log(i+"    :   "+ fName);
	// console.log(i+"    :   "+ options.uri);
	rp(options).then(function (data) {
		console.log(data)
		if(data){
			parsePageData(data, callBack, fName, );
		}else{
			callBack();
		}
	}).catch(function (err) {
		console.log(err);      
	});
}

function parsePageData(data, callBack, fName){
	console.log("parsePageData")
	var json = {}, text, arr = [];
	data(".middle-col").children().each(function(i, elem) {
		tagName = data(elem)[0].name;
		//console.log(tagName)
		if(tagName != "div" && tagName != "hr"){
			if((tagName == "h1") ||(tagName == "h2") ||(tagName == "h3") ||(tagName == "h4") ||(tagName == "h5") ||(tagName == "h6")){
				var obj = {};
				obj.tagName = tagName;
				obj.title = data(elem).text();
				// console.log(obj.title)
				arr.push(obj);
			}else{
				var obj = arr.pop();
				var txt = obj.description ? obj.description : [];
				var objNew = {};
				objNew.tagName = tagName;
				if((tagName == "ul") || (tagName == "table")){// || (tagName == "") || (tagName == ""))
					objNew.content = data(elem).html();
				}else if(tagName == "img"){
					objNew.content = data(elem).attr("src");
				}else{
					objNew.content = data(elem).text();
				}
				txt.push(objNew);
				obj.description = txt;
				// console.log(objNew.content)
				arr.push(obj);
			}
		}
	});
	json.arr = arr;
	fs.writeFile(dir+fName+'.json', JSON.stringify(json, null, 4), function(err){
		//console.log('File successfully     :    '+ fName);      
	});
	//Insert page content data
	MongoClient.connect(mongoDBurl, function(err, db) {
		if (err) throw err;
		var dbo = db.db(mongoDBname);
		var myobj = { type : "content", title:pageTitle, value: json};
		dbo.collection(languageName).insertOne(myobj, function(err, res) {
		  if (err) throw err;
		  console.log(ii + "   -    " + languageNames[jj] + "   -  "+jj+"   -    " + languageName + "    -    " + pageTitle + "     :    Page document inserted");
		  db.close();
		  
		  getAllLinkDataFromLangLoop(callBack);
		});
	});
}


//Database activity




app.listen('8081')

console.log('Magic happens on port 8081');

exports = module.exports = app;